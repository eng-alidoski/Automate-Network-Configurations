# Prerequisites

This repository contains a variety of Cisco IOS technologies configurations for Cisco routers and switches. You'll find configurations for Layer 3 technologies such as routing protocols, IP services, and VLANs, as well as Layer 2 technologies such as spanning tree protocol (STP), VLAN Trunking Protocol (VTP), and Link Aggregation Control Protocol (LACP).

# Automating the Backup of Running and Startup Configurations

Feel free to use these configurations as a starting point for your own Cisco IOS devices. You can either copy and paste the configurations directly into your devices, or use them as a reference to build your own configurations.

# Steps

- Collecting facts about the local host.
- Creating a directory to store the backup configurations.
- Pulling the running and startup configurations from the network devices.
- Creating subdirectories for the running and startup configurations.
- Copying the running and startup configurations to disk.
- Pulling the running and startup configurations from the backup directory.
- Using the Napalm module to push the running and startup configurations to the network device


