# Automating Network Configurations

The primary purpose of this repository is to document my progress throughout my network automation journey, as well as share my knowledge with the networking community.

Throughout this repository, I aim to automate the majority of the networking concepts I have learned throughout my CCNA/CCNP and Fortinet Security certification journeys.

I will be creating templates in YAML format, utilizing Ansible Modules, and implementing Jinja Templates to streamline the automation process.

So let's keep learning and automating!
