routing_protocol = "BGP"

# if routing_protocol == "OSPF" or "IS-IS":
#     print("This device is running a link state protocol")
# else:
#     print("This device is not running a link state protocol")

if routing_protocol in ["EIGRP", "OSPF", "BGP"]:
    print("This protocol is supported")
else:
    print("This protocol is not supported")

My_Protocols = ["RIP", "EIGRP", "ISIS"]

if "BGP" not in ["EIGRP", "OSPF", "BGP"]:
    print("This protocol is supported")
else:
    print("BGP is not supported")