#Python script that you can use to copy files created on a particular day to a specific folder

import os
import shutil
from datetime import date

# Define the source and destination directories
src_dir = "/home/alidoski/Python-Net-Eng"
dst_dir = "/home/alidoski/Automate-Network-Configurations/Python/Practice_Files"

# Get today's date
today = date.today()

# Loop through all the files in the source directory
for filename in os.listdir(src_dir):
    # Get the creation time of the file
    creation_time = os.path.getctime(os.path.join(src_dir, filename))
    creation_date = date.fromtimestamp(creation_time)
    
    # If the file was created today, copy it to the destination directory
    if creation_date == today:
        shutil.copy(os.path.join(src_dir, filename), dst_dir)
